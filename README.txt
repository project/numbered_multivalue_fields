
-- SUMMARY --

This is a small module which enables you to number Multiple Value Fields.

This can be helpful when you expect a large quantity of values per field. Also 
when you have large form items within each item e.g., Field Collection Items.

For a full description of the module, visit the project page:
  http://drupal.org/project/numbered_multivalue_fields

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/numbered_multivalue_fields


-- REQUIREMENTS --

Fields


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure settings in each field instance using the Field UI.
Structure » Content Types » Manage Fields:

  - Check the 'Numbered MultiValue Field' checkbox. Note - This will only be 
    visible when your field is set to have more than one field value.


-- CONTACT --

Current maintainers:
* Oliver Castle (ocastle) - https://www.drupal.org/user/1798580

This project has been sponsored by:
* Fullbundle
  Creative Drupal agency providing Drupal sites. Visit 
http://www.fullbundle.com for more information.
